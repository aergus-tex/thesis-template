# name of the final PDF file
# Don't forget to adjust this in `.gitlab-ci.yml` too!
NAME=thesis

# can be used with xelatex, lualatex or pdflatex
PDFLATEX=xelatex

# stuff required for building sections separately
SECTIONS_PATH=$(wildcard src/sections/*.tex)
SECTIONS_TEX=$(notdir $(SECTIONS_PATH))
SECTIONS_PDF=$(SECTIONS_TEX:%.tex=%.pdf)

default: draft

draft: $(NAME)-draft.pdf
release: $(NAME).pdf
sections: $(addprefix sections/,$(SECTIONS_PDF))
all: draft release sections

$(NAME)-draft.pdf: src/*.tex src/sections/*.tex src/*.sty src/*.bib
	cd src ; \
        if which latexmk >/dev/null ; then \
          latexmk -dvi- -pdf -pdflatex=$(PDFLATEX) \
            -latexoption="-synctex=1" main.tex ; \
        else \
          $(PDFLATEX) -synctex=1 -interaction=scrollmode main ; \
          bibtex main ; \
          mkindex main.idx ; \
          $(PDFLATEX) -synctex=1 -interaction=scrollmode main ; \
          $(PDFLATEX) -synctex=1 main ; \
        fi
	cp src/main.pdf $(NAME)-draft.pdf


$(NAME).pdf: clean-release
	git clone --recursive . .release
	sed -i "s/\RequirePackage\[mark\]{gitinfo2}/\RequirePackage{gitinfo2}/g" \
          .release/src/n002-thesis.cls
	cd .release && make
	cp .release/src/main.pdf $(NAME).pdf

sections/%.pdf: src/sections/%.tex prepare-sections
	cat src/utilities/section.prefix > sections/$(notdir $<)
	cat $< >> sections/$(notdir $<)
	cat src/utilities/section.suffix >> sections/$(notdir $<)
	cd sections ; \
	if which latexmk >/dev/null ; then \
          latexmk -dvi- -pdf -pdflatex=$(PDFLATEX) \
            -latexoption="-synctex=1" $(notdir $<) ; \
        else \
          $(PDFLATEX) -synctex=1 -interaction=scrollmode $(notdir $<) ; \
          bibtex $(notdir $<) ; \
          mkindex $(notdir $<) ; \
          $(PDFLATEX) -synctex=1 -interaction=scrollmode $(notdir $<) ; \
          $(PDFLATEX) -synctex=1 $(notdir $<) ; \
        fi

prepare-sections:
	mkdir -p sections
	cp -d src/*.bib sections/
	cp -d src/*.cls sections/
	cp -d src/*.sty sections/

clean: clean-release clean-sections
	rm -f src/*.aux src/*.idx src/*.ilg src/*.ind src/*.log src/*.nav ; \
	rm -f src/*.out src/*.snm src/*.toc src/*.vrb ; \
	rm -f src/*.bbl src/*.blg src/*.brf ; \
	rm -f src/*.fdb_latexmk src/*.fls ; \
	rm -f src/*.dvi src/*.pdf src/*synctex.gz* ; \
	rm -rf *.pdf

clean-release:
	rm -rf .release

clean-sections:
	rm -rf sections

.PHONY: clean clean-release clean-sections $(NAME).pdf
