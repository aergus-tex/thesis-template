;; Compilation Shortcuts

;; http://www.emacswiki.org/emacs/CompileCommand
(defun recompile-quietly ()
  "Re-compile without changing the window configuration."
  (interactive)
  (save-window-excursion
    (recompile)))

;; https://www.gnu.org/software/emacs/manual/html_node/efaq/Binding-keys-to-commands.html
(global-set-key [f5] (quote recompile-quietly))
(global-set-key [f6] (quote compile))
