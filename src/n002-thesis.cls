% n002-thesis.cls - part of a LaTeX template
%
% Written in 2015-2016 by Aras Ergus <arasergus@posteo.net>.
%
% This file is available for use under the terms of CC0, in particular in
% public domain where possible. For more information, please see:
% http://creativecommons.org/publicdomain/zero/1.0/

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{n002-thesis}[2016/11/28]

\LoadClass[11pt, a4paper, twoside, english]{article}

\RequirePackage{n002-things}
\RequirePackage{n002-math}

\RequirePackage[titletoc]{appendix}
\RequirePackage[english]{babel}
\RequirePackage{etoolbox}
\RequirePackage[mark]{gitinfo2}
\RequirePackage{imakeidx}
\RequirePackage[nottoc]{tocbibind}

\numberwithin{equation}{section}

\renewcommand{\gitMarkPref}{}

\renewcommand{\gitMark}{
  \gitAuthorDate{}
  \textbullet{}
  \gitHash{}
  \textbullet{}
}

\newbool{n002-bachelor}
\newbool{n002-master}
\newbool{bama-title}

\DeclareOption{bachelor}{
  \booltrue{n002-bachelor}
  \booltrue{bama-title}
}
\DeclareOption{master}{
  \booltrue{n002-master}
  \booltrue{bama-title}
}

\ProcessOptions\relax

\newcommand*{\advisor}[1]{\def\@advisor{#1}}
\newcommand*{\secondadvisor}[1]{\def\@secondadvisor{#1}}

\newcommand*{\dateofbirth}[1]{
  \ifbool{bama-title}{\geburtsdatum{#1}}{}
}
\newcommand*{\placeofbirth}[1]{
  \ifbool{bama-title}{\geburtsort{#1}}{}
}
\newcommand*{\institute}[1]{
  \ifbool{bama-title}{\institut{#1}}{}
}

\ifbool{n002-bachelor}{
  \RequirePackage{BA_Titelseite}
  \ausarbeitungstyp{Bachelorarbeit Mathematik}
  \betreuer{Betreuer: \@advisor}
  \zweitgutachter{Zweitgutachter: \@secondadvisor}
}{}
\ifbool{n002-master}{
  \RequirePackage{MA_Titlepage}
  \authornew{\@author}
  \ausarbeitungstyp{Master's Thesis Mathematics}
  \betreuer{Advisor: \@advisor}
  \zweitgutachter{Second Advisor: \@secondadvisor}
}{}

\endinput
