# Thesis Template

A template for (mathematics) theses (at University of Bonn).

## BaMa Files

This repository contains certain files provided by the [Bachelor-Master
office][bama] of the mathematics department at University of Bonn. See
`src/BA_Titelseite.sty` and `src/MA_Titlepage.sty` for details.

## Git Mark

In order to make git marks provided by [gitinfo2][gitinfo2] work properly,
you will need to add post-commit, post-merge etc. hooks running
[this][gitinfo-script].


[bama]: http://www.mathematics.uni-bonn.de/study/contact
[gitinfo2]: https://www.ctan.org/pkg/gitinfo2
[gitinfo-script]: http://mirrors.ctan.org/macros/latex/contrib/gitinfo2/post-xxx-sample.txt

